#!/usr/bin/env ruby

require 'pp'
require 'yaml'

$LOAD_PATH << File.dirname(__FILE__)
require 'ruby-dacp'
require 'config'

LASTFMSUBMITD_DIR = '/var/spool/lastfm'

def submit(track, time)
	File.open("#{LASTFMSUBMITD_DIR}/dacp#{track.trackId}-#{time.to_i}", 'w') do |file|
		file.puts '---'
		file.puts "artist: \"#{track.artist.gsub('"', '\"')}\""
		file.puts "title: \"#{track.title.gsub('"', '\"')}\""
		file.puts "album: \"#{track.album.gsub('"', '\"')}\""
		file.puts "length: #{track.totalTime / 1000}"
		file.puts "time: !timestamp #{time.utc.strftime('%Y-%m-%d %H:%M:%S')}"
	end
	puts 'submitted'
end

$client = DACPClient.new($host, $port)

lastTrackSubmittedId = nil
revision = nil
submitThread = nil
loop do
	sleep(2) #Maybe this will stop it seeing songs as being stopped during the track change
	status = $client.playstatusupdate(revision)
	revision = status.revision
	
	puts "#{status.title} / #{status.artist}: #{status.album}	(#{status.genre})#{formatTime(status.elapsedTime)}/#{formatTime(status.totalTime)}" + (status.state == DACPClient::PLAY_STATE_PLAYING ? '' : " (#{DACPClient::PLAY_STATE_NAMES[status.state]})")
	
	#Stop old submission thread before starting the new one
	if !submitThread.nil? && submitThread.alive?
		submitThread[:running] = false
		submitThread.run
		submitThread.join
		submitThread = nil
	end
	
	if status.state == DACPClient::PLAY_STATE_PLAYING && !status.totalTime.nil? && status.totalTime > 0 && status.elapsedTime >= 0 && status.elapsedTime < [status.totalTime / 2, 240000].min && status.trackId != lastTrackSubmittedId
		#Thread to submit track to last.fm when it reaches halfway through
		submitThread = Thread.new(status) do |status|
			Thread.current[:running] = true
			
			#Wait until halfway through the track
			puts "sleep #{([status.totalTime / 2, 240000].min - status.elapsedTime) / 1000}"
			status.elapsedTime += sleep(([status.totalTime / 2, 240000].min - status.elapsedTime) / 1000) * 1000
			
			if Thread.current[:running]
				puts "Submitting at #{formatTime(status.elapsedTime)}/#{formatTime(status.totalTime)}"
				submit(status, Time.now - status.elapsedTime / 1000)
				lastTrackSubmittedId = status.trackId
			else
				puts "Thread dying"
			end
		end
	end
end
