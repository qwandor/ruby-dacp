USER_CONFIG = "#{ENV['HOME']}/.ruby-dacp-config.rb"

$host = 'localhost'
$port = 3688

if File.file?(USER_CONFIG)
	require USER_CONFIG
end

$host = ENV['STEREO_HOST'] if ENV['STEREO_HOST']
$port = ENV['STEREO_PORT'] if ENV['STEREO_PORT']
