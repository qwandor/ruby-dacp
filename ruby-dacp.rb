require 'net/http'

def assert(result, message = nil)
	if !result
		$stderr.puts "Assertion failed"
		$stderr.puts ' ' + message if !message.nil?
		raise 'Assertion failed'
	end
end

#Turn time in milliseconds into m:ss
def formatTime(msTime)
	return '' if msTime.nil?
	
	secs = msTime / 1000
	sign = if secs < 0
		secs = -secs
		'-'
	else
		''
	end
	sprintf('%s%d:%02d', sign, secs / 60, secs % 60)
end

def readInt(string)
	(string[0] << 24) + (string[1] << 16) + (string[2] << 8) + string[3]
end

def readNode(string)
	type = string[0..3]
	length = (string[4] << 24) + (string[5] << 16) + (string[6] << 8) + string[7]
	body = readBody(string[8..7 + length], type)
	rest = string[8 + length..-1]
	
	[type, body, rest]
end

TypeAndNode = Struct.new('TypeAndNode', :type, :body)

def readBody(string, type)
	case type
	when 'cmst', 'caci', 'mlit', 'apso', 'avdb', 'aply', 'cmgt', 'mupd' #Map of nodes (no duplicate node types)
		list = {}
		while !string.empty?
			child_type, body, string = readNode(string)
			assert(!list.has_key?(child_type), "Duplicate child node #{child_type} in #{type}")
			list[child_type] = body
		end
		list
	when 'mlcl' #List of nodes
		list = []
		while !string.empty?
			child_type, body, string = readNode(string)
			list << TypeAndNode.new(child_type, body)
		end
		list
	when 'cmsr', 'caas', 'caar', 'mstt', 'cmmk', 'cant', 'cast', 'cmvo', 'mtco', 'mrco', 'mlid', 'astm', 'assr', 'miid', 'asst', 'assp', 'mimc', 'mctc', 'meds', 'mpco', 'mcti', 'musr' #int
		assert(string.length == 4)
		readInt(string)
	when 'caps', 'carp', 'cash', 'cmik', 'cmsp', 'cmsv', 'cass', 'casu', 'mikd', 'muty' #byte
		assert(string.length == 1)
		string[0]
	when 'asai', 'mper' #long (64 bit) int
		assert(string.length == 8)
		(readInt(string[0..3]) << 32) + readInt(string[4..7])
	when 'asbr' #short int (2 bytes)
		assert(string.length == 2)
		(string[0] << 8) + string[1]
	when 'canp' #4 ints
		assert(string.length == 16)
		[readInt(string[0..3]), readInt(string[4..7]), readInt(string[8..11]), readInt(string[12..15])]
	when 'cann', 'cana', 'canl', 'cang', 'ascp', 'minm', 'asar', 'asgn', 'asal' #string
		string
	when 'abpl', 'aeSP' #boolean
		assert(string.length == 1)
		assert(string[0] == 0 || string[0] == 1)
		string[0] != 0
	else
		$stderr.puts "Unknown node type #{type}, returning string #{string.inspect}"
		string
	end
end

def readMessage(string)
	type, body, rest = readNode(string)
	assert(rest.empty?)
	TypeAndNode.new(type, body)
end

PlayStatus = Struct.new('PlayStatus', :album, :artist, :title, :genre, :state, :revision, :totalTime, :elapsedTime, :trackId)
Playlist = Struct.new('Playlist', :size, :tracks)
PlaylistTrack = Struct.new('PlaylistTrack', :artist, :album, :title, :genre, :composer, :trackId, :albumId, :sampleRate, :totalTime, :containerItemId, :parentContainerId)
Update = Struct.new('Update', :status, :revision)

class Playlist
	def +(other)
		Playlist.new(size + other.size, tracks + other.tracks)
	end
end

class DACPClient
	DACPClient::PLAY_STATE_NAMES = [nil, nil, 'stopped', 'paused', 'playing']
	DACPClient::PLAY_STATE_STOPPED = 2
	DACPClient::PLAY_STATE_PAUSED = 3
	DACPClient::PLAY_STATE_PLAYING = 4
	DACPClient::SONG_FIELD_NAMES = {
		:artist => 'daap.songartist',
		:album => 'daap.songalbum',
		:title => 'dmap.itemname',
		:genre => 'daap.songgenre',
		:composer => 'daap.songcomposer',
		:id => 'dmap.itemid',
		:containerItemId => 'dmap.containeritemid',
	}
	
	def initialize(host, port)
		@http = Net::HTTP.new(host, port)
	end
	
	def playstatusupdate_raw(revision = nil)
		data = @http.get('/ctrl-int/1/playstatusupdate' + (revision.nil? ? '' : "?revision-number=#{revision}")).body
		
		parsed = readMessage(data)
		
		PlayStatus.new(parsed.body['canl'], parsed.body['cana'], parsed.body['cann'], parsed.body['cang'], parsed.body['caps'], parsed.body['cmsr'], parsed.body['cast'], parsed.body['cant'].nil? ? nil : parsed.body['cast'] - parsed.body['cant'], parsed.body['canp'].nil? ? nil : parsed.body['canp'][3])
	end
	
	def update(revision = nil)
		data = @http.get("/update?revision-number=#{revision || 0}").body
		parsed = readMessage(data)
		Update.new(parsed.body['mstt'], parsed.body['musr'])
	end
	
	def playstatusupdate(revision = nil)
		status = nil
		#Keep trying until playstatusupdate_raw returns successfully (it times out after waiting for a minute so we have to try again)
		loop do
			begin
				status = playstatusupdate_raw(revision)
				break
			rescue Timeout::Error
				#Do nothing, just loop through and try playstatusupdate_raw again
			end
		end
		
		status
	end
	
	def playlist(id = nil)
		data = @http.get(id ? "/databases/1/containers/#{id}/items" : '/ctrl-int/1/playlist').body
		
		parsed = readMessage(data)
		
		Playlist.new(parsed.body['mtco'], parsed.body['mlcl'].map {|track|
			PlaylistTrack.new(track.body['asar'], track.body['asal'], track.body['minm'], track.body['asgn'], track.body['ascp'], track.body['miid'], track.body['asai'], track.body['assr'], track.body['astm'], track.body['mcti'], track.body['mpco'])
		})
	end
	
	def play()
		@http.get('/ctrl-int/1/playpause')
	end
	
	def pause()
		@http.get('/ctrl-int/1/pause')
	end
	
	def stop()
		@http.get('/ctrl-int/1/stop')
	end
	
	def nextitem()
		@http.get('/ctrl-int/1/nextitem')
	end
	
	def previtem()
		@http.get('/ctrl-int/1/previtem')
	end
	
	def search(pattern, field = :all)
		escaped_pattern = URI.escape(pattern)
		if field == :all
			data = @http.get("/databases/1/containers/1/items?query='daap.songartist:#{escaped_pattern}','daap.songalbum:#{escaped_pattern}','dmap.itemname:#{escaped_pattern}','daap.songgenre:#{escaped_pattern}','daap.songcomposer:#{escaped_pattern}'").body
		elsif SONG_FIELD_NAMES.has_key?(field)
			data = @http.get("/databases/1/containers/1/items?query='#{SONG_FIELD_NAMES[field]}:#{escaped_pattern}'").body
		else
			raise ArgumentError, "Unrecognised field #{field}"
		end
		
		parsed = readMessage(data)
		Playlist.new(parsed.body['mtco'], parsed.body['mlcl'].map {|track|
			PlaylistTrack.new(track.body['asar'], track.body['asal'], track.body['minm'], track.body['asgn'], track.body['ascp'], track.body['miid'], track.body['asai'], track.body['assr'], track.body['astm'])
		})
	end
	
	#Add the results of a search to the playlist
	def cue_search(pattern, field = :all)
		escaped_pattern = URI.escape(pattern)
		if field == :all
			@http.get("/ctrl-int/1/cue?query='daap.songartist:#{escaped_pattern}','daap.songalbum:#{escaped_pattern}','dmap.itemname:#{escaped_pattern}','daap.songgenre:#{escaped_pattern}','daap.songcomposer:#{escaped_pattern}'")
		elsif SONG_FIELD_NAMES.has_key?(field)
			@http.get("/ctrl-int/1/cue?query='#{SONG_FIELD_NAMES[field]}:#{escaped_pattern}'")
		else
			raise ArgumentError, "Unrecognised field #{field}"
		end
	end
	
	#Remove an item from a given playlist by index, and return information about the track that was removed
	def remove_upcoming(playlist_id, index)
		playlist = playlist(playlist_id)
		track = playlist.tracks[index]
		playlist_id ||= track.parentContainerId
		remove(playlist_id, track.containerItemId, :containerItemId)
		track
	end
	
	#Remove a list of items from a given playlist by index, and return information about the tracks that were removed
	def remove_upcoming_list(playlist_id, indices)
		playlist = playlist(playlist_id)
		indices.map do |index|
			track = playlist.tracks[index]
			remove(playlist_id || track.parentContainerId, track.containerItemId, :containerItemId)
			track
		end
	end
	
	#Remove from a given playlist songs matching a given pattern
	def remove(playlist_id, pattern, field)
		escaped_pattern = URI.escape(pattern.to_s)
		@http.get("/databases/1/containers/#{playlist_id}/edit?action=remove&edit-params='#{SONG_FIELD_NAMES[field]}:#{pattern}'")
	end
	
	#In the given playlist, move the item at index_to_move to the new position new_index
	#Return information about the track that was moved
	def move_indexed(playlist_id, index_to_move, new_index)
		playlist = playlist(playlist_id)
		item_to_move = playlist.tracks[index_to_move]
		to_above = new_index >= playlist.size - 1 ? nil : playlist.tracks[new_index + (index_to_move < new_index ? 1 : 0)]
		playlist_id ||= (to_above || playlist.tracks[playlist.size - 1]).parentContainerId
		move(playlist_id, item_to_move.containerItemId, to_above ? to_above.containerItemId : 0)
		item_to_move
	end
	
	#In the given playlist, move the item identified by item_to_move to the position above another item identified by to_above
	#If to_above == 0, move to the end of the playlist
	def move(playlist_id, item_to_move, to_above)
		@http.get("/databases/1/containers/#{playlist_id}/edit?action=move&edit-params='edit-param.move-pair:#{item_to_move},#{to_above}'")
	end
	
	#Shuffle the order of songs in the given playlist
	#Return new (shuffled) playlist
	def shuffle(playlist_id)
		playlist = playlist(playlist_id)
		playlist.tracks.shuffle!
		(0..playlist.size - 2).reverse_each do |i|
			move(playlist_id, playlist.tracks[i].containerItemId, playlist.tracks[i + 1].containerItemId)
		end
		playlist
	end
	
	def get_property(name)
		data = @http.get("/ctrl-int/1/getproperty?properties=#{name}").body
		
		parsed = readMessage(data)
		parsed.body
	end
	
	def set_property(name, value)
		@http.get("/ctrl-int/1/setproperty?#{name}=#{value}")
	end
	
	def get_volume()
		get_property('dmcp.volume')['cmvo']
	end
	
	def set_volume(value)
		set_property('dmcp.volume', value)
	end
	
	#Tell stereo to connect to the given DAAP server
	def connect(host, port = nil)
		@http.get("/connect?host=#{host}" + (port ? "&port=#{port}" : ''))
	end
end
