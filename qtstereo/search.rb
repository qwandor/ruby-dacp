class Ui_SearchDialog < Qt::Dialog
	slots 'search()', 'enqueueMatches()', 'searchChanged()'
	
	def initialize(client, parent = nil)
		super(parent)
		require 'ui_search'
		setupUi(self)
		
		@client = client
		
		@matchesTable.setColumnCount(PLAYLIST_FIELDS.size)
		@matchesTable.setHorizontalHeaderLabels(PLAYLIST_FIELD_NAMES)
		
		@typeCombo.addItem('any field')
		@typeCombo.addItems(DACPClient::SONG_FIELD_NAMES.keys.map{|name| name.to_s})
	end
	
	##Slots
	def search()
		field = @typeCombo.currentText()
		field = 'all' if field == 'any field'
		matches = @client.search(@patternText.text(), field.to_sym)
		
		@matchesTable.setRowCount(matches.size)
		matches.tracks.each_with_index do |track, row|
			PLAYLIST_FIELDS.each_with_index do |field, column|
				value = field == :totalTime ? formatTime(track[field]) : track[field]
				item = Qt::TableWidgetItem.new(value)
				@matchesTable.setItem(row, column, item)
			end
		end
		
		@enqueueButton.setEnabled(true)
	end
	
	def enqueueMatches()
		field = @typeCombo.currentText()
		field = 'all' if field == 'any field'
		matches = @client.cue_search(@patternText.text(), field.to_sym)
		
		accept() #Close the dialog box
	end
	
	def searchChanged()
		#Type or pattern for search has been changed, disable enqueue button (until search is performed)
		@enqueueButton.setEnabled(false)
	end
end
