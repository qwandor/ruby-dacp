#!/usr/bin/env ruby

require 'pp'
require 'Qt4'
require 'main'

$LOAD_PATH << File.dirname(__FILE__)
require '../ruby-dacp'
require '../config'

$client = DACPClient.new($host, $port)

app = Qt::Application.new(ARGV)
mainWindow = Ui_MainWindow.new()
mainWindow.show

Thread.new do
	lastTrackId = nil
	revision = nil
	displayThread = nil
	loop do
		#Update playlists and volume
		mainWindow.setPlaylists($client.playlist(7), $client.playlist(4), $client.playlist(10))
		mainWindow.setVolume($client.get_volume())
		
		#Wait for playstatusupdate (this happens on track change, state change, &c.)
		status = $client.playstatusupdate(revision)
		revision = status.revision
		
		#Stop old display thread before starting the new one
		if !displayThread.nil?
			displayThread[:running] = false
			displayThread.join
		end
		
		#Thread to display information, updating elapsed time
		displayThread = Thread.new(status) do |status|
		 Thread.current[:running] = true
			#Keep incrementing the time (if the track is playing) for 10 seconds or until the end of the track (whichever is sooner) without contacting the server, to minimise load on the server while maintaining the appearance of a real-time display
			while Thread.current[:running] do
				mainWindow.setCurrent(status)
				
				status.elapsedTime += 1000 if status.state == 4
				break if !status.totalTime.nil? && (status.elapsedTime > status.totalTime || status.elapsedTime < 0)
				sleep(1)
			end
		end
	end
end

app.exec
