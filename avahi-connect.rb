#!/usr/bin/env ruby

require 'rubygems'
require 'pp'
require 'dbus'

$LOAD_PATH << File.dirname(__FILE__)
require 'ruby-dacp'
require 'config'

AVAHI_IF_UNSPEC = -1
AVAHI_PROTO_INET = 0
AVAHI_PROTO_INET6 = 1
AVAHI_PROTO_UNSPEC = -1

$client = DACPClient.new($host, $port)

$bus = DBus::SystemBus.instance
avahi_service = $bus.service('org.freedesktop.Avahi')
$avahi_server = avahi_service.object("/")
$avahi_server.introspect
$avahi_server.default_iface = 'org.freedesktop.Avahi.Server'

def handle_browse_signal(interface, protocol, name, type, domain, flags)
  puts "New server '#{name}' on interface #{interface}, protocol #{protocol}, resolving..."
  if protocol != AVAHI_PROTO_INET
    puts "  '#{name}' is not IPv4, ignoring for now"
  else
    begin
      interface, protocol, name, type, domain, host, aprotocol, address, port, txt, flags = $avahi_server.ResolveService(interface, protocol, name, type, domain, AVAHI_PROTO_INET, 0)
      puts "  Telling stereo to connect to #{host}:#{port}"
      $client.connect(host, port)
      puts "  done"
    rescue DBus::Error
      puts "  error resolving '#{name}', ignoring"
    end
  end
end

#Catch ItemNew signals here, even before we are ready to handle them, and remember them for later when we are up and going
itemnew_matchrule = DBus::MatchRule.new()
itemnew_matchrule.interface = 'org.freedesktop.Avahi.ServiceBrowser'
itemnew_matchrule.member = 'ItemNew'
itemnew_matchrule.type = 'signal'
queued_browse_signals = []
ready = false
$bus.add_match(itemnew_matchrule) do |m|
  interface, protocol, name, type, domain, flags = m.params
  if ready
    handle_browse_signal(interface, protocol, name, type, domain, flags)
  else
    queued_browse_signals << [interface, protocol, name, type, domain, flags]
  end
end

browser_path = $avahi_server.ServiceBrowserNew(AVAHI_IF_UNSPEC, AVAHI_PROTO_UNSPEC, '_daap._tcp', 'local', 0)[0]
avahi_browser = avahi_service.object(browser_path)
avahi_browser.introspect
avahi_browser.default_iface = 'org.freedesktop.Avahi.ServiceBrowser'

ready = true
queued_browse_signals.each do |params|
  handle_browse_signal(params[0], params[1], params[2], params[3], params[4], params[5])
end

loop = DBus::Main.new
loop << $bus
loop.run
