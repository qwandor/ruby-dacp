#!/usr/bin/env ruby

$LOAD_PATH << File.dirname(__FILE__)
require 'ruby-dacp'
require 'config'
require 'net/http'
require 'uri'
require 'rexml/document'
require 'json'

include REXML

FETCH = '/home/stereo/fetch'
SAY = '/home/stereo/say'
BACKGROUND_MUSIC = '/home/stereo/61321_mansardian_News_Background.wav'
END_MUSIC = '/home/stereo/61322_mansardian_News_End_Signature.wav'

hour = Time.new.hour
timeofday = hour < 12 ? 'morning' : (hour < 19 ? 'afternoon' : 'evening')

DJS = [
	'crystal',
	'mike',
	'rich',
	'lauren',
	'claire',
	'charles',
	'audrey',
	'anjali',
]

$dj = DJS[rand(DJS.length)]

GREETINGS = [
	'You are listening to Memphis Stereo.',
	"You are listening to Memphis Stereo. I am #{$dj}, your DJ for the hour.",
	"This is Memphis Stereo, and I am #{$dj}.",
	"Good #{timeofday} Memphis! This is Memphis Stereo.",
	"Good #{timeofday} Memphis! I am #{$dj} for Memphis Stereo.",
]

AFTER_NEWS = [
	'That was Memphis Headline News.',
	'And that was the headlines.',
	'That was the headlines.',
	'That was Memphis Headline News, brought to you by stuff dot co dot N Z.',
]

FAREWELL = [
	'Tune in next hour to hear more headlines.',
	'Thanks for listening to Memphis stereo.',
	'That\'s me for now, more headlines next hour.',
	'That\'s all for now, more next hour.',
	"Have a good #{timeofday}, and keep listening to Memphis Stereo.",
	"Enjoy the rest of the #{timeofday}; this is Memphis Stereo.",
	"#{$dj} out, enjoy the music.",
	"That's all from me, enjoy the music and have a great #{timeofday}.",
]

CATCHPHRASE = [
	'Memphis news, on the hour, every hour.',
	'Memphis news, on the hour, every hour.',
	'Memphis news, as it happens, when you need it.',
	'Memphis news, interrupting your music since 2009.',
]

SPONSORS = [
	'And now a word from our sponsors',
	'But first, a word from Drinkscorp.',
	'But first, I want to talk about fridge.',
	'But first, this hour\'s fridge product.',
]

def shell_quote(string)
	string.gsub('$', '\\$').gsub('"', '\\"')
end

def say(string, who = $dj)
	puts "#{who}: #{string}"
	system("#{SAY} \"#{shell_quote(string)}\" #{who}")
end

$queued_to_say = []

#Fetch the given phrase and add it to the queue to be said later
def say_later(string, who = $dj)
	system("#{FETCH} \"#{shell_quote(string)}\" #{who}")
	$queued_to_say << [string, who]
end

#Play all queued phrases
def say_queued()
	$queued_to_say.each do |line|
		say(line[0], line[1])
	end
	$queued_to_say = []
end

#Choose a string at random from the list, and say it
def say_one(strings, who = $dj)
	say(strings[rand(strings.length)], who)
end

def say_one_later(strings, who = $dj)
	say_later(strings[rand(strings.length)], who)
end

#Look for the same artist repeated at least 3 times.
def find_repeated_artist()
	playlist = $client.playlist()
	artist = nil
	repeats = 0
	playlist.tracks.each do |track|
		if track.artist == artist
			repeats += 1
			if repeats >= 2
				return artist
			end
		else
			artist = track.artist
			repeats = 0
		end
	end

	nil
end

$aplay_pid = nil

#Start playing the given sound file in the background, recording the process id playing it so that it can later be killed. Stop any previous music first
def start_music(filename, repeat = false)
	stop_music()
	if repeat
		$aplay_pid = `play -q "#{filename}" "#{filename}" "#{filename}" "#{filename}" "#{filename}" "#{filename}" "#{filename}" "#{filename}" vol 0.4 > /dev/null & echo $!`.to_i
	else
		$aplay_pid = `play -q "#{filename}" vol 0.4 > /dev/null & echo $!`.to_i
	end
end

#Stop any (background, not stereo) music which may be playing
def stop_music()
	if $aplay_pid
		begin
			Process.kill('TERM', $aplay_pid)
		rescue Errno::ESRCH
			#Already dead, no need to worry
		end
		$aplay_pid = nil
	end
end

##Preload as much as possible before waiting for stereo to change and pausing the music
StockInfo = Struct.new("StockInfo", :code, :name, :stock, :price)

fridge_items = []
Net::HTTP.get('sandpit.ecs.vuw.ac.nz', '/~fridge/quickstock.php?mode=tsv').split("\n").each do |line|
	parts = line.split("\t")
	fridge_items << StockInfo.new(parts[0], parts[1], parts[2].to_i, parts[3])
end

say_one_later GREETINGS

#News from stuff.co.nz
newsxml = Document.new(Net::HTTP.get(URI.parse('http://www.stuff.co.nz/rss/')))
new_headlines = []
newsxml.elements.each('rss/channel/item') do |item|
	title = item.elements['title'].text
	description = item.elements['description'].text
	
	new_headlines << "#{title}: #{description}"
end
old_headlines = File.open('news_old', 'r') do |file|
	file.readlines.map { |line| line.chomp }
end
headlines = new_headlines.reject { |headline| old_headlines.include?(headline) }
File.open('news_old', 'w') do |file|
	file.puts new_headlines
end

if !headlines.empty?
	say_later "This hour's news headlines are"
	sleep(0.3)
	headlines.each do |headline|
		say_later headline.gsub('&apos;', "'").gsub('&amp;', ' and ').gsub('&nbsp;', ' ').gsub('&rsquo;', "'").gsub('&#39;', "'")
	end
	sleep(0.5)
	say_one_later AFTER_NEWS
end

#Weather
weather_page = Net::HTTP.get(URI.parse('http://metservice.co.nz/webData/localForecastWellington'))
if weather_data = JSON.parse(weather_page)
	min = weather_data['days'][0]['min']
	max = weather_data['days'][0]['max']
	forecast = weather_data['days'][0]['forecast']
	say_later 'The current weather forecast for today is'
	say_later forecast
	#sleep(0.4) #Does not work when saying later
	say_later "The high and low temperatures for today are #{max}"
	say_later "and #{min}"
	say_later 'degrees centigrade'
end

#Load quote data
quotesxml = Document.new(Net::HTTP.get(URI.parse("http://thequotebook.net/contexts/1/quotes.xml")))
quotes = quotesxml.get_elements('quotes/quote')
quote = quotes[rand(quotes.length)]
##Big preload ends here

$client = DACPClient.new($host, $port)

status = $client.playstatusupdate
if status.state == DACPClient::PLAY_STATE_PLAYING
	#Wait for the song to end, then pause it
	status = $client.playstatusupdate(status.revision)
end

if was_playing = (status.state == DACPClient::PLAY_STATE_PLAYING)
	$client.pause
end

start_music(BACKGROUND_MUSIC, true)

say_queued()

if was_playing
	say "Up next is #{status.title} by #{status.artist}."
end

sleep(0.5)

#Advertise a fridge product
stocked_fridge_items = fridge_items.select { |item| item.stock > 0 }
fridge_item = stocked_fridge_items[rand(stocked_fridge_items.length)]
say_one_later SPONSORS
say_later "Have you tried #{fridge_item.name}? The fridge has #{fridge_item.stock} left in stock, for only $#{fridge_item.price} each.", 'lauren'
say_queued()

start_music(END_MUSIC)

#Memphis quote
say_later "#{quote.elements['quotee/fullname'].text} once said"
say_later "#{quote.elements['quote-text'].text}", 'mike'
say_queued()

sleep(0.5)

if repeated_artist = find_repeated_artist()
	say "Clearly someone really likes #{repeated_artist}."
end

if was_playing
	$client.play
end

stop_music()

say_one FAREWELL

say_one CATCHPHRASE, 'lauren'
