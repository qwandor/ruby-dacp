#!/usr/bin/env ruby

require 'pp'
require 'rubygems'
require 'ruport'

$LOAD_PATH << File.dirname(__FILE__)
require 'ruby-dacp'
require 'config'

ANSI_UP = "\e[A"
ANSI_ERASE_LINE = "\e[2K"
ANSI_ERASE_DOWN = "\e[J"

def shorten(string, max_length)
	if string.nil?
		''
	elsif string.length > max_length
		string[0..max_length - 4] + '...'
	else
		string
	end
end

#Print a list of tracks in a nice table
#The first column shows either the track id, or the result of a block to which the track is passed
def print_list(matches)
	table = Ruport::Data::Table.new(:column_names => ['', 'Track', 'Artist', 'Album', 'Genre', 'Length'])
	matches.tracks.each do |track|
		table << [block_given? ? yield(track) : track.trackId, shorten(track.title, 30), shorten(track.artist, 30), shorten(track.album, 30), track.genre, formatTime(track.totalTime)]
	end
	puts table
	
	puts "#{matches.size} tracks total"
end

class Command
	attr_reader :name, :description
	attr_accessor :help_text
	@@commands = {}
	
	def initialize(name, description)
		@name = name
		@description = description
		@help_text = "Usage: #{$0} #{name}\n"
		@@commands[name] = self
	end
	
	def self.commands
		@@commands
	end
	
	def run(args)
		puts "Sorry, the #{@name} command is not yet implemented."
	end
end

List = Command.new('list', 'lists the playlist')
def List.run(args)
	#If no playlist number is given, display the special 'current' playlist (past and future songs), and point out which one is currently playing
	status = $client.playstatusupdate() if args.empty?
	playlist = $client.playlist(args.empty? ? nil : args[0].to_i)
	
	number = -1
	print_list(playlist) do |track|
		number += 1
		(args.empty? && status.trackId == track.trackId ? '**' : number.to_s) + (args.empty? && track.parentContainerId == 7 ? '+' : '')
	end
end
List.help_text = <<EOF
Usage: #{$0} list [<playlist number>]
If no playlist number is given then show the special 'current' playlist.
EOF

Play = Command.new('play', 'starts playback')
def Play.run(args)
	$client.play()
end

Stop = Command.new('stop', 'stops playback')
def Stop.run(args)
	$client.stop()
end

Toggleplay = Command.new('toggleplay', 'toggles playback status between play/pause')
Pause = Command.new('pause', 'pause playback')
def Pause.run(args)
	$client.pause()
end

Next = Command.new('next', 'play next song')
def Next.run(args)
	$client.nextitem()
	Current.run([])
end

Prev = Command.new('prev', 'play previous song')
def Prev.run(args)
	$client.previtem()
	Current.run([])
end

Current = Command.new('current', 'formatted information about the currently playing track')
def Current.run(args)
	status = $client.playstatusupdate()
	
	puts status.title
	puts "#{status.artist}: #{status.album} (#{status.genre})"
	puts "#{formatTime(status.elapsedTime)} / #{formatTime(status.totalTime)}" if !status.totalTime.nil?
	puts DACPClient::PLAY_STATE_NAMES[status.state]
end
Current.help_text = <<EOF
Usage: #{$0} current
The output format is like this:
<track title>
<artist>: <album> (<genre>)
<elapsed track time> / <total track time>
EOF

Help = Command.new('help', 'print help about a command')
def Help.run(args)
	if args.empty?
		puts 'Available commands:'
		Command.commands.each do |name, command|
			puts "  #{command.name} - #{command.description}"
		end
	else
		if Command.commands.has_key?(args[0])
			command = Command.commands[args[0]]
			puts "  #{command.name} - #{command.description}"
			puts command.help_text if !command.help_text.nil?
		else
			$stderr.puts "No help for #{args[0]}"
		end
	end
end
Help.help_text = <<EOF
Usage: #{$0} help [<command name>]
If no command name is given a list of all available commands will be printed.
EOF

Status = Command.new('status', 'go into status mode')
def Status.run(args)
	long = !args.empty? && args[0] == 'long' #Long mode, print playlist as well as current song
	
	#Exit quietly on Ctrl-C, rather then spewing a backtrace; also make sure to clear the playlist shown in long mode
	trap('SIGINT') do
		puts ANSI_ERASE_DOWN
		exit(0)
	end
	
	lastTrackId = nil
	revision = nil
	displayThread = nil
	loop do
		status = $client.playstatusupdate(revision)
		revision = status.revision
		
		#Stop old display thread before starting the new one
		if !displayThread.nil?
			displayThread[:running] = false
			displayThread.join
		end
		
		if status.trackId != lastTrackId
			puts
			if long
				#Erase screen after cursor
				print ANSI_ERASE_DOWN
				#Show upcoming playlist
				playlist = $client.playlist(7) + $client.playlist(4)
				print_list(playlist) do |track|
					track.trackId.to_s + (track.parentContainerId == 7 ? '+' : '')
				end
				#Go back up to top, ready tho show current song
				print "\e[#{playlist.size + 5}A"
			end
		end
		
		lastTrackId = status.trackId
		
		#Thread to display information, updating elapsed time
		displayThread = Thread.new(status) do |status|
		 Thread.current[:running] = true
			#Keep incrementing the time (if the track is playing) for 10 seconds or until the end of the track (whichever is sooner) without contacting the server, to minimise load on the server while maintaining the appearance of a real-time display
			while Thread.current[:running] do
				puts "#{ANSI_UP}#{ANSI_ERASE_LINE}#{status.title} / #{status.artist}: #{status.album} (#{status.genre})	#{formatTime(status.elapsedTime)}/#{formatTime(status.totalTime)}" + (status.state == 4 ? '' : " (#{DACPClient::PLAY_STATE_NAMES[status.state]})")
				
				status.elapsedTime += 1000 if status.state == 4
				break if !status.totalTime.nil? && (status.elapsedTime > status.totalTime || status.elapsedTime < 0)
				sleep(1)
			end
		end
	end
end
Status.help_text = <<EOF
Usage: #{$0} status [long]
EOF

Search = Command.new('search', 'search the collection')
def Search.run(args)
	if args.length == 1
		field = :all
		pattern = args[0]
	elsif args.length == 2
		field = args[0].to_sym
		pattern = args[1]
	else
		$stderr.puts "Wrong number of arguments to search"
		puts help_text
		return
	end
	
	begin
		matches = $client.search(pattern, field)
	rescue ArgumentError => error
		$stderr.puts "Error: #{error}"
		return
	end
	
	print_list(matches)
end
Search.help_text = <<EOF
Usage: #{$0} search [artist|album|title|genre|composer|id] <pattern>
EOF

SearchAdd = Command.new('searchadd', 'search the collection, adding the results to the playlist')
def SearchAdd.run(args)
	if args.length == 1
		field = :all
		pattern = args[0]
	elsif args.length == 2
		field = args[0].to_sym
		pattern = args[1]
	else
		$stderr.puts "Wrong number of arguments to search"
		puts help_text
		return
	end
	
	begin
		$client.cue_search(pattern, field)
	rescue ArgumentError => error
		$stderr.puts "Error: #{error}"
		return
	end
end
SearchAdd.help_text = <<EOF
Usage: #{$0} searchadd [artist|album|title|genre|composer|id] <pattern>
EOF

Volume = Command.new('volume', 'display or set the volume')
def Volume.run(args)
	if args.length == 0
		puts $client.get_volume()
	elsif args.length == 1
		vol = args[0]
		old_volume = $client.get_volume()
		if vol[0..0] == '+'
			$client.set_volume(old_volume + vol[1..-1].to_i)
		elsif vol[0..0] == '-'
			$client.set_volume(old_volume - vol[1..-1].to_i)
		else
			$client.set_volume(vol.to_i)
		end
		new_volume = $client.get_volume()
		puts "#{old_volume} -> #{new_volume}"
	else
		$stderr.puts "Wrong number of arguments to volume"
		puts help_text
		return
	end
end
Volume.help_text = <<EOF
Usage: #{$0} volume [<value>|+<increase>|-<decrease>]
EOF

#Parse something like '1,3-5,7' into [1,3,4,5,7]
def parse_range(range)
	items = []
	range.split(',').each do |range|
		parts = range.split('-').map{|p| p.to_i }
		if parts.length == 1
			items << parts[0]
		elsif parts.length == 2
			items += (parts[0]..parts[1]).to_a
		else
			raise "Error parsing range: '#{range}' is not a valid range"
		end
	end
	
	items
end

Remove = Command.new('remove', 'remove a track from a playlist')
def Remove.run(args)
	if args.length == 1
		removed = $client.remove_upcoming_list(nil, parse_range(args[0]))
	elsif args.length == 2
		removed = $client.remove_upcoming_list(args[0].to_i, parse_range(args[1]))
	elsif args.length == 3
		$client.remove(args[0].to_i, args[2], args[1].to_sym)
		return #No list to print, unfortunately
	else
		$stderr.puts "Wrong number of arguments to remove"
		puts help_text
		return
	end
	
	removed.each do |item|
		puts "Removed '#{item.title} / #{item.artist}: #{item.album} (#{item.genre})'"
	end
end
Remove.help_text = <<EOF
Usage: #{$0} remove [<playlist number>] <range of offsets in playlist>
If no playlist given, remove from the special 'current' playlist.
A range can be something like '0,2-5,7,9-10'.
       #{$0} remove <playlist number> artist|album|title|genre|composer|id <pattern>
Remove all tracks matching the given search pattern in the given field.
EOF

Move = Command.new('move', 'move a track in a playlist to a new position')
def Move.run(args)
	if args.length == 2
		new_offset = args[1].to_i
		moved = $client.move_indexed(nil, args[0].to_i, new_offset)
	elsif args.length == 3
		new_offset = args[2].to_i
		moved = $client.move_indexed(args[0].to_i, args[1].to_i, new_offset)
	else
		$stderr.puts "Wrong number of arguments to move"
		puts help_text
		return
	end
	puts "Moved '#{moved.title}' to offset #{new_offset}"
end
Move.help_text = <<EOF
Usage: #{$0} move [<playlist number>] <old offset> <new offset>
If no playlist given, edit the special 'current' playlist.
EOF

Connect = Command.new('connect', 'tell stereo to add the given DAAP server as a source')
def Connect.run(args)
	if args.length == 1
		$client.connect(args[0])
	elsif args.length == 2
		$client.connect(args[0], args[1].to_i)
	else
		$stderr.puts "Wrong number of arguments to connect"
		puts help_text
		return
	end
end
Connect.help_text = <<EOF
Usage: #{$0} connect <hostname> [<port number>]
EOF

Shuffle = Command.new('shuffle', 'shuffle the tracks in a given playlist')
def Shuffle.run(args)
	if args.length == 1
		shuffled_playlist = $client.shuffle(args[0].to_i)
		print_list(shuffled_playlist)
	else
		$stderr.puts "Wrong number of arguments to shuffle"
		puts help_text
		return
	end
end
Shuffle.help_text = <<EOF
Usage: #{$0} shuffle <playlist number>
EOF

$client = DACPClient.new($host, $port)

if ARGV.empty?
	Help.run([])
else
	command_name = ARGV[0]
	if Command.commands.has_key?(command_name)
		Command.commands[command_name].run(ARGV[1..-1])
	else
		$stderr.puts "Unknown command #{command_name}"
		Help.run([])
	end
end
