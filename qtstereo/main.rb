require 'Qt4'
require 'search'

PLAYLIST_FIELDS = [:title, :artist, :album, :genre, :totalTime]
PLAYLIST_FIELD_NAMES = ['Track', 'Artist', 'Album', 'Genre', 'Length']
QUEUED_TEXT_COLOUR = Qt::Color.new(80, 50, 0)
QUEUED_BACKGROUND_COLOUR = Qt::Color.new(255, 250, 220)

class Ui_MainWindow < Qt::MainWindow
	slots 'timeout()', 'playPause()', 'stop()', 'next()', 'changeVolume(int)', 'remove()', 'showSearch()'
	
	def initialize(parent = nil)
		super
		require 'ui_main'
		setupUi(self)
		
		[@historyTable, @upcomingTable].each do |table|
			table.setColumnCount(PLAYLIST_FIELDS.size)
			table.setHorizontalHeaderLabels(PLAYLIST_FIELD_NAMES)
		end
		
		#For context menu
		@upcomingTable.addAction(@actionRemove)
		
		#Cannot just use $client from other thread, as problems occur when we try to do HTTP GETs while the other thread is waiting on one (for playstatusupdate)
		@client = DACPClient.new($host, $port)
		@state = DACPClient::PLAY_STATE_STOPPED
		
		@upcomingList = [] #Concatenation of queued and random playlists; used to work out what track to remove
		
		timer = Qt::Timer.new(self)
		connect(timer, SIGNAL('timeout()'), SLOT('timeout()'))
		timer.start(0)
	end
	
	##Slots
	def timeout()
		sleep 0.01 #To give Ruby threads a chance to run
	end
	
	def playPause()
		#Use separate thread so as not to block GUI
		Thread.new do
			if @state == DACPClient::PLAY_STATE_PLAYING
				@client.pause()
			else
				@client.play()
			end
		end
	end
	
	def stop()
		#Use separate thread so as not to block GUI
		Thread.new do
			@client.stop()
		end
	end
	
	def next()
		#Use separate thread so as not to block GUI
		Thread.new do
			@client.nextitem()
		end
	end
	
	def changeVolume(volume)
		#Use separate thread so as not to block GUI
		Thread.new do
			@client.set_volume(volume)
		end
	end
	
	def remove()
		index = @upcomingTable.currentRow()
		return if index == -1
		track = @upcomingList[index]
		#Use separate thread so as not to block GUI
		Thread.new do
			@client.remove(track.parentContainerId, track.containerItemId, :containerItemId)
		end
	end
	
	def showSearch()
		if @searchDialog.nil?
			@searchDialog = Ui_SearchDialog.new(@client)
		end
		@searchDialog.show()
		@searchDialog.raise()
		@searchDialog.activateWindow()
	end
	
	##Methods called by update thread
	def setCurrent(status)
		@currentSong.setText("#{status.title} / #{status.artist}: #{status.album} (#{status.genre})	#{formatTime(status.elapsedTime)}/#{formatTime(status.totalTime)}")
		@state = status.state #Used by playPause()
		@playButton.setText(status.state == DACPClient::PLAY_STATE_PLAYING ? '&Pause' : '&Play')
		@statusbar.showMessage(DACPClient::PLAY_STATE_NAMES[status.state])
		setWindowTitle("#{status.title} / #{status.artist}")
	end
	
	def setPlaylists(queued, random, recent)
		@upcomingList = queued.tracks + random.tracks
		@upcomingTable.setRowCount(queued.size + random.size)
		row = 0
		[queued, random].each_with_index do |playlist, playlistNumber|
			playlist.tracks.each do |track|
				PLAYLIST_FIELDS.each_with_index do |field, column|
					value = field == :totalTime ? formatTime(track[field]) : track[field]
					item = Qt::TableWidgetItem.new(value)
					if playlistNumber == 0
						item.setTextColor(QUEUED_TEXT_COLOUR)
						item.setBackgroundColor(QUEUED_BACKGROUND_COLOUR)
					end
					@upcomingTable.setItem(row, column, item)
				end
				row += 1
			end
		end
		
		@historyTable.setRowCount(recent.size)
		recent.tracks.each_with_index do |track, row|
			PLAYLIST_FIELDS.each_with_index do |field, column|
				value = field == :totalTime ? formatTime(track[field]) : track[field]
				item = Qt::TableWidgetItem.new(value)
				@historyTable.setItem(row, column, item)
			end
		end
	end
	
	def setVolume(volume)
		@volumeSlider.setValue(volume)
	end
end
